﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DynamicGUI
{
    /// <summary>
    /// This form in designed to generate the GUI of this application.
    /// </summary>
    public partial class frmDynamicForm : Form
    {
        #region private attributs
        private int amountOfDynamicButtonsRequested = 0;//
        #endregion privat attributs

        public frmDynamicForm()
        {
            InitializeComponent();
        }

        #region Event Handlers
        /// <summary>
        /// This method start the button duplication process
        /// </summary>
        /// <param name="sender">Control on which the user has clicked</param>
        /// <param name="e">Arguments related with the click event</param>
        private void cmdStart_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= this.amountOfDynamicButtonsRequested; i++)
            {
                DuplicateButton(i);
            }
            this.cmdStart.Enabled = false;//on désactive le bouton
            this.txtBQty.Enabled = false;//on désactive le champs texte
        }

        /// <summary>
        /// This method displays the button name
        /// </summary>
        /// <param name="sender">The button on which the user has clicked</param>
        /// <param name="e">Arguments related with the click event</param>
        private void cmdDuplicate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;//on cast le sender pour en obtenir une référence sur le bouton qui a été cliqué
            string buttonTxt = btn.Text;//on extrait le texte du bouton
            MessageBox.Show("Hello, I'm the button " + buttonTxt);//on affiche le message avec le nom du bouton
        }

        /// <summary>
        /// This method is called by the event handler "TextChanged" activated on a textBox
        /// * checks the input of the user
        /// * informs the user in case of unvalide input
        /// </summary>
        /// <param name="sender">Control on which the user has clicked</param>
        /// <param name="e">Arguments related with the click event</param>
        /// <returns>The quantity input and validate</returns>
        private void txtBQty_TextChanged(object sender, EventArgs e)
        {
            int qteInput = 0;//variable qui stockera la saisie, vérifiée, de l'utilisateur

            try
            {
                qteInput = int.Parse(this.txtBQty.Text);//on extrait la saisie utilisateur
                //soit la quantité saisie est à l'intérieur de l'intervalle
                if (qteInput > 0 && qteInput <= 10)
                {
                    cmdStart.Enabled = true;//on active le boutton permettant de lancer la création des bouttons
                    this.amountOfDynamicButtonsRequested = qteInput;//on stocke la valeur saisie dans l'attribut privé de la classe
                }
                //soit la quantité saisie est refusée (hors de l'intervalle)
                else
                {
                    cmdStart.Enabled = false;//désactiver le boutton
                    MessageBox.Show("Please input an integer (min 1 max 10)");//afficher le message
                }
            }
            catch (FormatException)//on intercepte une éventuelle erreur de format (voir l'opération int.Parse ci-dessus)
            {
                MessageBox.Show("Only integer values are accepted !");//afficher le message
            }
        }

        #endregion Event Handlers

        #region private methods
        /// <summary>
        /// This method duplicate the control button
        /// </summary>
        /// <param name="btnId">Is used to get the button name as well as his text</param>
        private void DuplicateButton(int btnId)
        {
            int gabBetween2Buttons = 5;//espace entre deux boutons
            int btnHeight = 162;//largeur bouton
            int btnWidth = 23;//hauteur bouton

            //on agrandit la fenêtre (allongement vers le bas)
            this.ClientSize = new System.Drawing.Size(186, 112 + (23 + gabBetween2Buttons) * btnId);

            //on créer le nouveau bouton
            Button cmdDynamic = new Button();//on déclare et instancie un nouveau boutton
            cmdDynamic.Location = new System.Drawing.Point(12, 53 + ((btnWidth + gabBetween2Buttons) * btnId));//on positionne le bouton
            cmdDynamic.Name = "cmdDynamic" + btnId;//on ajoute un nom au bouton
            cmdDynamic.Size = new System.Drawing.Size(btnHeight, btnWidth);//on dimensionne le bouton
            cmdDynamic.TabIndex = btnId;//on configure la valeur
            cmdDynamic.Text = "Dynamic" + btnId;//on définit le nom du bouton
            cmdDynamic.UseVisualStyleBackColor = true;//on définit le style du bouton

            //on ajoute un eventHandler pour permettre au bouton, si on lui clique dessus, de produire un message (méthode cmdDuplicate_click)
            cmdDynamic.Click += new System.EventHandler(cmdDuplicate_Click);

            //on ajoute le bouton au formulaire courant
            this.Controls.Add(cmdDynamic);
        }
        #endregion private methods
    }
}