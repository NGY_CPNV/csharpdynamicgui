﻿namespace DynamicGUI
{
    partial class frmDynamicForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdStart = new System.Windows.Forms.Button();
            this.txtBQty = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmdStart
            // 
            this.cmdStart.Enabled = false;
            this.cmdStart.Location = new System.Drawing.Point(12, 53);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(162, 23);
            this.cmdStart.TabIndex = 0;
            this.cmdStart.Text = "Duplicate";
            this.cmdStart.UseVisualStyleBackColor = true;
            this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
            // 
            // txtBQty
            // 
            this.txtBQty.Location = new System.Drawing.Point(83, 15);
            this.txtBQty.Name = "txtBQty";
            this.txtBQty.Size = new System.Drawing.Size(24, 20);
            this.txtBQty.TabIndex = 1;
            this.txtBQty.Text = "0";
            this.txtBQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBQty.TextChanged += new System.EventHandler(this.txtBQty_TextChanged);
            // 
            // frmDynamicForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(186, 112);
            this.Controls.Add(this.txtBQty);
            this.Controls.Add(this.cmdStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDynamicForm";
            this.Text = "Dynamic Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdStart;
        private System.Windows.Forms.TextBox txtBQty;
    }
}

